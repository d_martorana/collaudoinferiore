!define APPNAME "Collaudo Inferiore"
!define COMPANYNAME "ARCA"
!define VERSIONMAJOR 1
!define VERSIONMINOR 0
!define VERSIONBUILD 2
!define INSTFOLDER "C:\ARCA\Collaudo Inferiore"
!define SOURCEFOLDER "D:\Progetti VBNET\CM18\Collaudo Inferiore\Collaudo Inferiore\bin\Debug"
OutFile "${APPNAME} V${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}.exe"
;RequestExecutionLevel admin



Section "install"
	;Banner::show /set 76 "Banner Visualizzato" /set 54 "Normal Text"
	SetOutPath "${INSTFOLDER}"
	File "${SOURCEFOLDER}\Collaudo Inferiore.exe"
	File "${SOURCEFOLDER}\Italiano.txt"
	File "${SOURCEFOLDER}\Setup.ini"
	File "${SOURCEFOLDER}\Test_inferiore.ini"
	File "${SOURCEFOLDER}\DB\Man1220_dati.mdb"
	
	CreateShortCut "$DESKTOP\${APPNAME}.lnk" "File a cui fare riferimento" ""
	
	WriteUninstaller "${INSTFOLDER}\Uninstall.exe"
SectionEnd

Section "Uninstall"
	RMDir /r "${INSTFOLDER}\*.*"
	RMDir "${INSTFOLDER}"
SectionEnd

Function .onInstSuccess
	MessageBox MB_OK "Installazione eseguita correttamente!"
FunctionEnd

Function un.onUninstSuccess
	MessageBox MB_OK "Disinstallazione eseguita correttamente!"
FunctionEnd