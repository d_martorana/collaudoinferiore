﻿Imports System.Net.Sockets
Imports System.Text.UTF8Encoding


Module Lan_Comunication
    Private Listener As TcpListener
    Private Client As TcpClient
    Private NetStream As NetworkStream


    Public Sub Open_lan(ByVal Port As Integer)

        Dim ip As Net.IPAddress
        If Not Net.IPAddress.TryParse(IP_Macchina, ip) Then
            MessageBox.Show("IP non valido!", Principale.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        'Quindi inizializza un client e tenta la connessione
        'al dato IP sulla porta 5000

        Client = New TcpClient()

        Try
            Application.DoEvents()
            Client.Connect(ip, Port)
        Catch Ex As Exception

        End Try

        'Se la connessione ha avuto successo, ottiene il network
        'stream e agisce sui controlli come nel codice precedente

        If Client.Connected Then

            NetStream = Client.GetStream()

        Else

        End If
    End Sub


    Public Sub Close_Lan()
        If Not IsNothing(Client) Then
            If NetStream.CanWrite = True Then
                Dim Buffer() As Byte = UTF8.GetBytes("\close\")
                NetStream.Write(Buffer, 0, Buffer.Length)
            End If
            Client.Client.Close()
            Client.Close()
            Client = Nothing

            If Listener IsNot Nothing Then
                Listener.Server.Close()
                Listener = Nothing
            End If
        End If
    End Sub

    Public Function HI_lev_lan(ByVal OUT As String, Optional ByVal Tout As Long = 5000) As Boolean


        Dim Lan_data = False

        OUT = Chr(2) & OUT & Chr(3)
        Dim S As Integer = 0
        Dim D As Integer = 0
        Dim n As Integer = Len(OUT$)

        For aaa% = 1 To n% Step 1
            S% = (Asc(Mid$(OUT, aaa%, 1)))
            D% = (D% + S%)
        Next
        Dim r As Byte = (D% And &H7F)
        OUT = OUT + Chr(r) & Chr(4)

        If Not String.IsNullOrEmpty(OUT) Then
            Dim Buffer_send() As Byte = UTF8.GetBytes(OUT)

            NetStream.Write(Buffer_send, 0, Buffer_send.Length)

        End If

        Do While Client.Available = 0
            'Li legge dallo stream
            Application.DoEvents()
            'Li trasforma in una stringa
        Loop
        Dim Buffer_ric(Client.Available - 1) As Byte
        NetStream.Read(Buffer_ric, 0, Buffer_ric.Length)

        Ricez = UTF8.GetString(Buffer_ric)
        Ricez = Ricez.Substring(1, Ricez.Length - 4)

        Exit Function
fine:

    End Function

    Function String_to_Buff(ByVal Stringa As String) As Byte()
        Dim Buff(Stringa.Length) As Byte
        For a = 0 To Stringa.Length - 1
            Buff(a) = Asc(Stringa.Substring(a, 1))
        Next
        Return Buff
    End Function

    Function Send_lan(ByVal out As String)
        Dim Buffer_send() As Byte = String_to_Buff(out)
        Dim Buffer_sen() As Byte = ASCII.GetBytes(out)
        NetStream.Write(Buffer_send, 0, Buffer_send.Length)

        Do While Client.Available = 0
            'Li legge dallo stream
            Application.DoEvents()
            'Li trasforma in una stringa
        Loop
        Dim Buffer_ric(Client.Available - 1) As Byte
        NetStream.Read(Buffer_ric, 0, Buffer_ric.Length)

        Ricez = UTF8.GetString(Buffer_ric)
    End Function
End Module

