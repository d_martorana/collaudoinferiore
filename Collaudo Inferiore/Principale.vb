﻿Imports System.Data.OleDb
Imports System
Imports System.IO
Imports System.Collections
Imports Microsoft.VisualBasic.FileIO
Imports System.Text
Imports System.Net.Sockets
Imports System.Text.UTF8Encoding
Imports Collaudo_Inferiore.ArcaClass
Imports Collaudo_Inferiore.CMClass
Imports Collaudo_Inferiore.CMCommandImport

Public Class frmPrincipale

    Public WorkDir As String
    Public Database_name As String
    Public Database_path As String
    Public Fw_Path As String
    Public IP_Macchina As String
    Public Comm_Port As String
    Public Zip_path As String
    Dim scala As UInteger = Me.Height / 754


    Private Sub Principale_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Application.Exit()
    End Sub

    Private Sub Principale_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Application.Exit()
    End Sub

    'Private Sub Principale_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    Tasto = e.KeyCode

    'End Sub

    Private Sub Principale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        WorkDir = Application.StartupPath & "\"
        Dim scala As UInteger = Me.Height / 754

        Database_name = Read_Setup("Server", "DatabaseName")
        Database_path = Read_Setup("Server", "Databasepath")
        Fw_Path = Read_Setup("Server", "FWpath")
        IP_Macchina = Read_Setup("comunicazione", "Ip_macchina")
        Comm_Port = Read_Setup("comunicazione", "Com")
        lingua = Read_Setup("lingua", "lingua")
        Label1.Width = Me.Width - 50
        Label1.Top = 50
        Label1.Left = 25
        ConnectionParam.ConnectionMode = DLINKMODESERIAL
        ConnectionParam.RsConf.baudrate = 9600
        ConnectionParam.RsConf.device = "COM" & Comm_Port
        ConnectionParam.RsConf.stopbit = 1
        ConnectionParam.RsConf.parity = 0
        ConnectionParam.RsConf.car = 8
        Combo_Id_prodotto.Width = 150 * scala
        Combo_cliente.Width = 400 * scala
        Combo_Id_prodotto.Top = 150 * scala
        Combo_cliente.Top = 150 * scala
        Combo_Id_prodotto.Left = (Me.Width - (Combo_Id_prodotto.Width + Combo_cliente.Width)) / 2
        Combo_cliente.Left = (Combo_Id_prodotto.Left + Combo_Id_prodotto.Width + 40)

        Button1.Top = (Me.Height - Button1.Height) / 2
        Button1.Left = (Me.Width - Button1.Width) / 2

        ProgressBar1.Top = 200 * scala
        ProgressBar1.Left = 100 * scala
        ProgressBar1.Width = Me.Width - (200 * scala)

        If verifica_folder() = False Then
            End
        End If

        ListBox1.Top = Me.Height - ListBox1.Height - 50
        Me.AcceptButton = Button1
        Me.Text = Application.ProductVersion
    End Sub



    Function verifica_folder() As Boolean

        Zip_path = Read_Setup("7-zip", "7-Zip_path")

        Dim aa = Dir(Zip_path & "7z.exe")

        If aa = "" Then
            ScriviMessaggio(Messaggio(92), Label1)
            AttendiTasto()
            verifica_folder = False
        Else
            verifica_folder = True
        End If

    End Function

    Sub Riempi_combo_Id_Prodotto()
        Dim Conn_String As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source =" & Database_path & Database_name
        'Dichiaro la nuova connessione
        Dim cn As New OleDbConnection(Conn_String)
        cn.Open()

        Dim sql As String
        sql = "SELECT DISTINCT tblman.id_prodotto FROM tblman, prodotti where tblman.id_prodotto = prodotti.id_prodotto and (prodotti.categoria_prodotto like 'CM18%' or prodotti.categoria_prodotto like 'CM20%')"

        Dim cmd As New OleDbCommand(sql, cn)

        Dim dr As OleDbDataReader = cmd.ExecuteReader

        Do While Not dr.Read() = Nothing
            Combo_Id_prodotto.Items.Add(dr(0))

        Loop
        cn.Close()
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Button1.Visible = False
        ScriviMessaggio(Messaggio(1), Label1) 'Scegliere il codice macchina e il cliente e premere invio
        Combo_Id_prodotto.Enabled = True
        Combo_Id_prodotto.Visible = True
        Combo_cliente.Enabled = True
        Combo_cliente.Visible = True
        Inferiore.Data = Now.ToString("d")
        Inferiore.Ora = Now.ToString("T")
        Combo_Id_prodotto.Focus()

    End Sub

    Private Sub Principale_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Crea_copia_locale()
        Database_path = WorkDir & "DB\"
        Fw_Path = WorkDir & "FW\"
        Riempi_combo_Id_Prodotto()
        ProgressBar1.Visible = False
        ScriviMessaggio("", Label1)
        Button1.Text = Messaggio(15)
        Button1.Visible = True

    End Sub

    Sub Crea_copia_locale()
        My.Computer.FileSystem.CreateDirectory(WorkDir & "DB")
        My.Computer.FileSystem.CreateDirectory(WorkDir & "FW")
        My.Computer.FileSystem.CreateDirectory(WorkDir & "Download")

        ScriviMessaggio(Messaggio(2), Label1) 'Copia del database in corso

        Application.DoEvents()
        My.Computer.FileSystem.CopyFile(Database_path & Database_name, WorkDir & "\Db\" & Database_name, overwrite:=True)
        ScriviMessaggio(Messaggio(3), Label1) 'Aggiornamento Database in corso...
        Dim Conn_String As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source =" & Database_path & Database_name
        'Dichiaro la nuova connessione
        Dim cn As New OleDbConnection(Conn_String)
        cn.Open()

        Dim cmd As New OleDb.OleDbCommand("SELECT COUNT (*) AS 'Count' FROM (SELECT Distinct cod_fw from tblman)", cn)

        Dim n_of_record As Integer = cmd.ExecuteScalar

        ProgressBar1.Maximum = n_of_record

        Dim sql As String
        sql = "SELECT DISTINCT tblman.cod_fw FROM tblman ORDER BY cod_fw"

        Dim cmd1 As New OleDbCommand(sql, cn)
        Dim conta = 0
        Dim dr As OleDbDataReader = cmd1.ExecuteReader

        Do While Not dr.Read() = Nothing
            Dim cur_dir As String = dr(0)
            Try
                If Directory.Exists(WorkDir & "FW\" & cur_dir) = False Then
                    My.Computer.FileSystem.CreateDirectory(WorkDir & "FW\" & cur_dir)
                    My.Computer.FileSystem.CopyDirectory(Fw_Path & cur_dir, WorkDir & "FW\" & cur_dir, True)
                End If
            Catch
            End Try
            conta = conta + 1
            ProgressBar1.Value = conta
            Application.DoEvents()
        Loop
        cn.Close()
    End Sub

    Private Sub Combo_Id_prodotto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Id_prodotto.SelectedIndexChanged
        Dim Conn_String As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source =" & Database_path & Database_name
        'Dichiaro la nuova connessione
        Dim cn As New OleDbConnection(Conn_String)
        cn.Open()

        Dim sql As String
        sql = "SELECT distinct nome_cliente FROM clienti, tblman where tblman.id_prodotto like '" & Combo_Id_prodotto.Text & "' and tblman.id_cliente = clienti.id_cliente"

        Dim cmd As New OleDbCommand(sql, cn)

        Combo_cliente.Items.Clear()

        Dim dr As OleDbDataReader = cmd.ExecuteReader
        Do While Not dr.Read() = Nothing
            Combo_cliente.Items.Add(dr(0))
        Loop
        cn.Close()
    End Sub

    Private Sub Combo_Id_prodotto_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_Id_prodotto.TextChanged
        Combo_cliente.Text = ""
    End Sub

    Private Sub Combo_cliente_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_cliente.SelectedIndexChanged
        Combo_cliente.Enabled = False
        Combo_Id_prodotto.Enabled = False
        ScriviMessaggio(Messaggio(4), Label1) 'Premere INVIO per confermare i dati, Esc per ripetere la selezione
        AttendiTasto()
        If Tasto = 27 Then
            Combo_Id_prodotto.Enabled = True
            Combo_cliente.Enabled = True
            Exit Sub
        End If
        Combo_Id_prodotto.Visible = False
        Combo_cliente.Visible = False
        Inferiore.Nome_cliente = Combo_cliente.Text
        Inferiore.Codice_Macchina = Combo_Id_prodotto.Text

        Dim Conn_String As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source =" & Database_path & Database_name
        Dim cn As New OleDbConnection(Conn_String)
        cn.Open()

        Dim sql As String
        sql = "SELECT id_cliente FROM clienti where clienti.nome_cliente = '" & Inferiore.Nome_cliente & "'"
        Dim cmd1 As New OleDbCommand(sql, cn)
        Dim dr As OleDbDataReader = cmd1.ExecuteReader
        dr.Read()
        Inferiore.Codice_cliente = dr(0)


        Dim cnn As New OleDbConnection(Conn_String)
        cnn.Open()

        sql = "SELECT DISTINCT prodotti.categoria_prodotto FROM prodotti where prodotti.id_prodotto = '" & Inferiore.Codice_Macchina & "'"
        Dim cmd2 As New OleDbCommand(sql, cnn)
        Dim drr As OleDbDataReader = cmd2.ExecuteReader
        drr.Read()
        Dim Macchina_categoria As String = UCase(drr(0))
        Inferiore.categoria = ""
        Dim aa As String = InStr(Macchina_categoria, "CRM")

        Inferiore.CRM = False
        If aa > 0 Then
            Inferiore.CRM = True
            Inferiore.categoria = Mid$(Macchina_categoria, 1, aa - 2)

        End If

        aa = InStr(Macchina_categoria, "CD80")
        Inferiore.CD80 = 0
        If aa > 0 Then
            Inferiore.CD80 = Mid$(Macchina_categoria, aa - 1, 1) * 2
            If Inferiore.categoria = "" Then Inferiore.categoria = Trim(Mid$(Macchina_categoria, 1, aa - 2))
        End If

        If Inferiore.categoria = "" Then Inferiore.categoria = Macchina_categoria


        cnn.Close()

        Call test_inferiore()
    End Sub

    Sub test_inferiore()
        ImpostaConnessione()
        Inferiore.Test.test = False
        Inferiore.setup_cassette = False
        Inferiore.assign = False
        Inferiore.Cassette_number = False
        Inferiore.Deposito = False
        Inferiore.Test.photo_sensor = False

        '        ScriviMessaggio(messaggio(62)) 'Inserire la matricola del trasporto in test e premere invio
        'ins_matr:
        '        Text1.Focus()
        '        Text1.Text = ""
        '        Attendi_Invio()
        '        If Text1.TextLength <> 12 Then
        '            ScriviMessaggio(messaggio(64) & Chr(13) & messaggio(62)) 'Inserire il codice del trasporto in test e premere invio
        '            AttendiTasto()
        '            GoTo ins_matr
        '        End If

        'Inferiore.Matricola_Inferiore = Text1.Text

        If Cassette_Number() = False Then
            GoTo fine
        End If
        Inferiore.Cassette_number = True

        If Assigne_Cassette() = False Then
            GoTo fine
        End If
        Inferiore.assign = True

        If Attendi_recovery() = False Then
            GoTo fine
        End If

        If AggiornaFwCD80() = False Then
            GoTo fine
        End If

        If Taratura_Foto() = False Then
            GoTo fine
        End If
        Inferiore.Test.photo_sensor = True

        If CD80_test() = False Then
            GoTo fine
        End If
        Inferiore.test_cd80 = True
        Traspout()


capacità:
        ' Limita capacità
        If LimitaCapacità() = False Then
            ScriviMessaggio(Messaggio(115), Label1)
            AttendiTasto()
            If Tasto = 27 Then GoTo fine
            GoTo capacità
        End If
        Inferiore.Test.RiduzioneCap = True

        'balanc:
        '        If Balance_set() = False Then
        '            ScriviMessaggio(messaggio(18))
        '            AttendiTasto()
        '            If Tasto = 27 Then GoTo fine
        '            GoTo balanc
        '        End If


Set_cas:
        If Setup_cassette(Me) = False Then
            ScriviMessaggio(Messaggio(48), Label1)
            AttendiTasto()
            If Tasto = 27 Then GoTo fine
            GoTo Set_cas
        End If
        Inferiore.setup_cassette = True
Mini_Dep:
        If Mini_Deposito() = False Then
            If Tasto = 27 Then GoTo fine
            GoTo Mini_Dep
        End If
        Inferiore.Deposito = True
        Inferiore.Test.test = True
        salva_dati()
        Scrivi_O(Messaggio(74))
        AttendiTasto()
fine:

        If Inferiore.Test.test = False Then
            salva_dati()
        End If
        Label1.Text = ""
        Button1.Visible = True
    End Sub

    Public Function LimitaCapacità() As Boolean
        LimitaCapacità = False
        Label1.Text = Messaggio(19) 'Entrata in transparent mode
        Traspin()
        'If Ricez.Substring(4) <> "101" And Ricez.Substring(4) <> "1" Then
        If comandoSingoloRisposta(2) <> "101" And comandoSingoloRisposta(2) <> "1" Then
            Exit Function
        End If
        Label1.Text = Messaggio(116) '"Riduzione capacità cassetto A in corso"
        'Traspsend("", 0, 1000)
        CmdTransparentCommand("940300020B0C")
        Label1.Text = Messaggio(117) 'Riduzione capacità cassetto B in corso
        'Traspsend("950300020B0C", 0, 1000)
        CmdTransparentCommand("950300020B0C")
        Label1.Text = Messaggio(118) 'Uscita dal transparent mode
        Traspout()
        'If Ricez.Substring(4) <> "101" And Ricez.Substring(4) <> "1" Then
        If comandoSingoloRisposta(2) <> "101" And comandoSingoloRisposta(2) <> "1" Then
            Exit Function
        End If
        'HiLevSend("T, 1, 0, 7")
        cmdComandoSingolo("T,1,0,7")
        MySleep(25000)
        Attendi_recovery()
        LimitaCapacità = True
    End Function
    Public Function AggiornaFwCD80() As Boolean
        AggiornaFwCD80 = False
        Label1.Text = "cancello la directory"
        Dim dir As DirectoryInfo = New DirectoryInfo(Application.StartupPath & "\Download")
        For Each file In dir.GetFiles
            file.Delete()
        Next
        Dim Conn_String As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source =" & Database_path & Database_name
        'Dichiaro la nuova connessione
        Dim cn As New OleDbConnection(Conn_String)
        cn.Open()
        Dim sql As String
        sql = "SELECT * FROM tblman WHERE id_prodotto = '" & Inferiore.Codice_Macchina & "' AND id_cliente = " & Inferiore.Codice_cliente & " AND tipo_fw = 4"

        Dim cmd As New OleDbCommand(sql, cn)

        Dim dr As OleDbDataReader = cmd.ExecuteReader
        Label1.Text = "Cerco il codice della suite"
        Do While Not dr.Read() = Nothing
            Inferiore.Codice_FW_Suite = (dr("cod_fw"))
        Loop
        cn.Close()
        Label1.Text = "codice suite: " & Inferiore.Codice_FW_Suite
        If Inferiore.Codice_FW_Suite = "" Then
            Label1.Text = "Nessuna suite trovata"
            AttendiTasto()
            Exit Function
        End If

        Dim startInfo As New ProcessStartInfo
        startInfo.FileName = Zip_path & "7z.exe"
        startInfo.Arguments = "e " & Chr(34) & Application.StartupPath & "\FW\" & Inferiore.Codice_FW_Suite & "\" & Inferiore.Codice_FW_Suite & ".zip" & Chr(34) & " -o" & Chr(34) & Application.StartupPath & "\Download\" & Chr(34)
        Dim pStart As Process = Process.Start(startInfo)
        pStart.WaitForExit()
        Dim fileFw As String = ""
        dir = New DirectoryInfo(Application.StartupPath & "\Download")
        Label1.Text = "Cerco il fw del CD80 nella cartella: " & Application.StartupPath & "\Download"
        For Each file In dir.GetFiles
            Me.Text = file.Name.Substring(0, 4)
            'MySleep(500)
            If file.Name.Substring(0, 4) = "CD80" Then
                fileFw = file.Name
                Label1.Text = "Trovato il fw: " & fileFw
                Exit For
            End If
        Next
        If fileFw = "" Then
            Label1.Text = "Non ho trovato il fw del CD80 nella cartella: " & Application.StartupPath & "\Download"
            AttendiTasto()
            Exit Function
        End If

        Label1.Text = "Verifica FW installato"
        Dim fileVer, fileRel, instVer, instRel As String
        'Dim instFile As String()
        'Dim deliminatore As Char = ","c

        cmdComandoSingolo("V,1,A")
        'HiLevSend("V,1,A")
        'instFile = Ricez.Split(deliminatore)
        fileVer = fileFw.Substring(5, 4)
        fileRel = fileFw.Substring(10, 4)

        instVer = comandoSingoloRisposta(3)
        instRel = comandoSingoloRisposta(4)
        Label1.Text = "File da installare:" & fileVer & "_" & fileRel & vbNewLine & "Fw installato:" & instVer & "_" & instRel
        MySleep(2000)
        'If fileVer = instFile(3) And fileRel = instFile(4) Then
        If fileVer = instVer And fileRel = instRel Then
            GoTo fine
        End If

        Label1.Text = "Installazione FW " & fileFw
        cmdComandoSingolo("L,1,A,F,,,,,\STORAGE CARD\" & fileFw, 2, 500000)
        'HiLevSend("L,1,A,F,,,,,\STORAGE CARD\" & fileFw)
        MySleep(1000)
        'Select Case Ricez.Substring(4)
        Select Case comandoSingoloRisposta(2)
            Case "985"
                Label1.Text = "File non trovato"
                Exit Function
            Case "203"
                Label1.Text = "Risposta:SW ERROR. Non è possibile installare il FW."
                Exit Function
            Case "201"
                Label1.Text = "Comando errato, FW non installato"
                Exit Function
            Case Else
                'Label1.Text = Ricez & vbNewLine & "Premi un tasto ad installazione finita"
                'AttendiTasto()
        End Select
        'Label1.Text = "Premi un tasto ad installazione finita"
        'AttendiTasto()
        Label1.Text = "Attendo recovery"
        Attendi_recovery()
fine:
        Inferiore.Test.FwCD80 = fileFw
        AggiornaFwCD80 = True
    End Function
    Sub salva_dati()
        Dim Stringa_salvataggio As String = ""
        Dim stringa_intestazione As String = ""
        Dim Stringa_foto As String = ""

        stringa_intestazione = Inferiore.Codice_Macchina & " | " & Inferiore.Codice_cliente & " | " & Inferiore.Matricola_Inferiore & " | " & Inferiore.Data & " | " & Inferiore.Ora & " | " & Inferiore.Test.test & " | " & Application.ProductVersion.ToString & " | " & System.Windows.Forms.SystemInformation.ComputerName & " | " & Inferiore.Test.RiduzioneCap & " | " & Inferiore.Test.FwCD80 & " | " & Inferiore.MatricolaCassA & " | " & Inferiore.MatricolaCassB & " | "

        For a = 0 To 12
            Try
                Stringa_foto = Stringa_foto & Inferiore.Test.cassette_photo(a).Foto_in & " | "
                Stringa_foto = Stringa_foto & Inferiore.Test.cassette_photo(a).Foto_Out & " | "
            Catch
                Stringa_foto = Stringa_foto & " | "
                Stringa_foto = Stringa_foto & " | "
            End Try
        Next

        Stringa_salvataggio = stringa_intestazione & Inferiore.Cassette_number & " | " & Inferiore.assign & " | " & Inferiore.Test.photo_sensor & " | " & Stringa_foto & Inferiore.test_cd80 & " | " & Inferiore.setup_cassette & " | " & Inferiore.Deposito

        Dim path As String = WorkDir & "Inferiore.txt"
        Dim sw As StreamWriter

        If File.Exists(path) = False Then
            ' Create a file to write to.
            sw = File.CreateText(path)
            sw.WriteLine(Stringa_salvataggio)
            sw.Flush()
            sw.Close()
        Else
            sw = File.AppendText(path)
            sw.WriteLine(Stringa_salvataggio)
            sw.Flush()
            sw.Close()
        End If
    End Sub

    Function Mini_Deposito() As Boolean
inizio:
        Mini_Deposito = True
        Dim cass_max As Byte

        Select Case Inferiore.categoria
            Case "CM18B"
                cass_max = 6
            Case "CM18", "CM20S", "CM18EVO"
                cass_max = 8
            Case "CM20"
                cass_max = 10
            Case "CM18T", "CM18EVOT", "CM20T"
                cass_max = 12
        End Select



        ScriviMessaggio(Messaggio(49), Label1) 'Primo deposito: Inserire una mazzetta di 10 banconote per taglio e premere un tasto

        AttendiTasto()
        ScriviMessaggio(Messaggio(50), Label1) 'Deposito in corso...
ridep:
        Dim k As Byte = 0
        Call Open_R(True)

        'Call HiLevSend("D,1,R,0,0000", "")
        cmdComandoSingolo("D,1,R,0,0000", 3, 65000)
        'Call Interpreta_deposito(Ricez$)
        Interpreta_deposito("")
        If Inferiore.CD80 > 0 Then
            If cass_max - Inferiore.CD80 < 7 Then
                k = 7 - (cass_max - Inferiore.CD80)
            Else
                k = 0
            End If
        End If

        If Deposito.Banconote_Rif <> 0 Or Deposito.Banconote_UNFit <> k * 10 Then
            Mini_Deposito = False
            ScriviMessaggio(Messaggio(51), Label1) 'Deposito fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
            AttendiTasto()
            If Tasto = 27 Then Exit Function
            GoTo inizio

        End If

        If Deposito.RC = "15" Then GoTo ridep

        Dim cass_num As Byte = 0
        For a = Inferiore.CD80 To cass_max - 1

            If Deposito.Bn_cas(a).N_note >= 10 Then
                cass_num = cass_num + 1
            End If

        Next

        If Inferiore.categoria = "CM18B" Then
            If cass_num <> 6 Then

                Mini_Deposito = False
                ScriviMessaggio(Messaggio(51), Label1) 'Deposito fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo inizio

            End If

        Else
            If cass_max - Inferiore.CD80 >= 7 Then
                If cass_num <> 7 Then

                    Mini_Deposito = False
                    ScriviMessaggio(Messaggio(51), Label1) 'Deposito fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
                    AttendiTasto()
                    If Tasto = 27 Then Exit Function
                    GoTo inizio

                End If
            Else
                If cass_num <> cass_max - Inferiore.CD80 Then

                    Mini_Deposito = False
                    ScriviMessaggio(Messaggio(51), Label1) 'Deposito fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
                    AttendiTasto()
                    If Tasto = 27 Then Exit Function
                    GoTo inizio

                End If
            End If
        End If

        ScriviMessaggio(Messaggio(52), Label1) 'Secondo deposito: Inserire una mazzetta di 10 banconote per taglio e premere un tasto

        AttendiTasto()

        ScriviMessaggio(Messaggio(50), Label1) 'Deposito in corso...

        Call Open_R(True)

        'Call HiLevSend("D,1,R,0,0000", "")
        cmdComandoSingolo("D,1,R,0,0000", 3, 65000)
        'Call Interpreta_deposito(Ricez$)
        Interpreta_deposito("")
        If Inferiore.CD80 > 0 Then

            If cass_max - Inferiore.CD80 < 7 Then
                k = 7 - (cass_max - Inferiore.CD80)
            Else
                k = 0
            End If
        End If

        If Deposito.Banconote_Rif <> 0 Or Deposito.Banconote_UNFit <> k * 10 Then

            Mini_Deposito = False
            ScriviMessaggio(Messaggio(51), Label1) 'Deposito fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
            AttendiTasto()
            If Tasto = 27 Then Exit Function
            GoTo inizio
        End If

        cass_num = 0
        For a = Inferiore.CD80 To cass_max - 1

            If Deposito.Bn_cas(a).N_note >= 10 Then
                cass_num = cass_num + 1
            End If

        Next

        If Inferiore.categoria = "CM18B" Then
            If cass_num <> 6 Then

                Mini_Deposito = False
                ScriviMessaggio(Messaggio(51), Label1) 'Deposito fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo inizio

            End If

        Else
            If cass_max - Inferiore.CD80 >= 7 Then
                If cass_num <> 7 Then

                    Mini_Deposito = False
                    ScriviMessaggio(Messaggio(51), Label1) 'Deposito fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
                    AttendiTasto()
                    If Tasto = 27 Then Exit Function
                    GoTo inizio

                End If
            Else
                If cass_num <> cass_max - Inferiore.CD80 Then

                    Mini_Deposito = False
                    ScriviMessaggio(Messaggio(51), Label1) 'Deposito fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
                    AttendiTasto()
                    If Tasto = 27 Then Exit Function
                    GoTo inizio

                End If
            End If
        End If

        ScriviMessaggio(Messaggio(53), Label1) 'Premere un tasto per avviare il prelievo...
        AttendiTasto()

        If Prelievo_30_bn() = False Then
            Mini_Deposito = False
            If Tasto = 27 Then Exit Function
            GoTo inizio

        End If

        If Inferiore.CD80 > 0 Then
cd80:
            If Svuotamento_CD80() = False Then
                Mini_Deposito = False
                If Tasto = 27 Then Exit Function
                GoTo inizio
            End If

            If Reset_cd80() = False Then
                GoTo cd80
            End If
        End If

        Mini_Deposito = True

    End Function

    Function Reset_cd80() As Boolean
        ScriviMessaggio(Messaggio(108), Label1) 'Registrazione matricola BOXA in corso
        'Call HiLevSend("Z,1,A,4," & Inferiore.CD80_Matricola_A, "")
        cmdComandoSingolo("Z,1,A,4," & Inferiore.CD80_Matricola_A, 4)

        'If Microsoft.VisualBasic.Right(Ricez, 2) = ",1" Or Microsoft.VisualBasic.Right(Ricez, 3) = "101" Then
        If comandoSingoloRisposta(4) = "1" Or comandoSingoloRisposta(4) = "101" Then
        Else
            ScriviMessaggio(Messaggio(112), Label1) 'Registrazione matricola FALLITA! Premere un tasto per ripetere, ESC per uscire.
            AttendiTasto()
            Reset_cd80 = False
            Exit Function
        End If

        ScriviMessaggio(Messaggio(109), Label1) 'Registrazione matricola BOXB in corso
        'Call HiLevSend("Z,1,B,4," & Inferiore.CD80_Matricola_B, "")
        cmdComandoSingolo("Z,1,B,4," & Inferiore.CD80_Matricola_B, 4)

        'If Microsoft.VisualBasic.Right(Ricez, 2) = ",1" Or Microsoft.VisualBasic.Right(Ricez, 3) = "101" Then
        If comandoSingoloRisposta(4) = "1" Or comandoSingoloRisposta(4) = "101" Then
        Else
            ScriviMessaggio(Messaggio(112), Label1) 'Registrazione matricola FALLITA! Premere un tasto per ripetere, ESC per uscire.
            AttendiTasto()
            Reset_cd80 = False
            Exit Function
            Reset_cd80 = False
        End If

        If Inferiore.CD80 = 4 Then
            ScriviMessaggio(Messaggio(110), Label1)
            'Call HiLevSend("Z,1,C,4," & Inferiore.CD80_Matricola_C, "")
            cmdComandoSingolo("Z,1,C,4," & Inferiore.CD80_Matricola_C, 4)
            'If Microsoft.VisualBasic.Right(Ricez, 2) = ",1" Or Microsoft.VisualBasic.Right(Ricez, 3) = "101" Then
            If comandoSingoloRisposta(4) = "1" Or comandoSingoloRisposta(4) = "191" Then

            Else
                ScriviMessaggio(Messaggio(112), Label1)
                AttendiTasto()
                Reset_cd80 = False
                Exit Function
                Reset_cd80 = False
            End If

            ScriviMessaggio(Messaggio(111), Label1)
            'Call HiLevSend("Z,1,D,4," & Inferiore.CD80_Matricola_D, "")
            cmdComandoSingolo("Z,1,D,4," & Inferiore.CD80_Matricola_D, 4)

            'If Microsoft.VisualBasic.Right(Ricez, 2) = ",1" Or Microsoft.VisualBasic.Right(Ricez, 3) = "101" Then
            If comandoSingoloRisposta(4) = "1" Or comandoSingoloRisposta(4) = "191" Then
            Else
                ScriviMessaggio(Messaggio(112), Label1)
                AttendiTasto()
                Reset_cd80 = False
                Exit Function
                Reset_cd80 = False
            End If
        End If

        Extended_Stat()

        cash_data()

        If Ext_Status.Cassette1.status <> "05" Then
            ScriviMessaggio(Messaggio(113), Label1)
            AttendiTasto()
            Reset_cd80 = False
            Exit Function
            Reset_cd80 = False
        End If

        If Cash_dat.Bn_cas(0).N_note <> 0 Then
            ScriviMessaggio(Messaggio(113), Label1)
            AttendiTasto()
            Reset_cd80 = False
            Exit Function
            Reset_cd80 = False
        End If

        If Ext_Status.Cassette2.status <> "05" Then
            ScriviMessaggio(Messaggio(113), Label1)
            AttendiTasto()
            Reset_cd80 = False
            Exit Function
            Reset_cd80 = False
        End If

        If Cash_dat.Bn_cas(1).N_note <> 0 Then
            ScriviMessaggio(Messaggio(113), Label1)
            AttendiTasto()
            Reset_cd80 = False
            Exit Function
            Reset_cd80 = False
        End If

        If Inferiore.CD80 = 4 Then
            If Ext_Status.Cassette3.status <> "05" Then
                ScriviMessaggio(Messaggio(113), Label1)
                AttendiTasto()
                Reset_cd80 = False
                Exit Function
                Reset_cd80 = False
            End If

            If Cash_dat.Bn_cas(2).N_note <> 0 Then
                ScriviMessaggio(Messaggio(113), Label1)
                AttendiTasto()
                Reset_cd80 = False
                Exit Function
                Reset_cd80 = False
            End If

            If Ext_Status.Cassette4.status <> "05" Then
                ScriviMessaggio(Messaggio(113), Label1)
                AttendiTasto()
                Reset_cd80 = False
                Exit Function
                Reset_cd80 = False
            End If

            If Cash_dat.Bn_cas(3).N_note <> 0 Then
                ScriviMessaggio(Messaggio(113), Label1)
                AttendiTasto()
                Reset_cd80 = False
                Exit Function
                Reset_cd80 = False
            End If

        End If

        Reset_cd80 = True

    End Function

    Function Prelievo_30_bn() As Boolean

inizio:
        Prelievo_30_bn = False
        ScriviMessaggio(Messaggio(54), Label1) 'Prelievo in corso...
        If Inferiore.categoria = "CM18B" Then
            'Call HiLevSend("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CP**,20", "")
            cmdComandoSingolo("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CP**,20", 3, 65000)
        Else
            'Call HiLevSend("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CPIA,10,CPJA,10", "")
            cmdComandoSingolo("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CPIA,10,CPJA,10", 3, 65000)
        End If
        If Controlla_prelievo(10) = False Then
            ScriviMessaggio(Messaggio(65), Label1) 'Prelievo fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
            AttendiTasto()
            Exit Function
        End If
        ScriviMessaggio(Messaggio(66), Label1) 'Togliere la mazzetta dalla bocchetta di uscita e premere un tasto

        AttendiTasto()

        If Inferiore.CD80 > 0 Then

            If Inferiore.CD80 = 2 Then
                'Call HiLevSend("w,1,R,0,A,CPCA,10,CPJA,10", "")
                cmdComandoSingolo("w,1,R,0,A,CPCA,10,CPJA,10", 3, 65000)

                If Controlla_prelievo(10) = False Then
                    ScriviMessaggio(Messaggio(98), Label1) 'Prelievo fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
                    AttendiTasto()
                    Exit Function
                End If
                'Call HiLevSend("w,1,R,0,B,CPDA,10,CPIA,10", "")
                cmdComandoSingolo("w,1,R,0,B,CPDA,10,CPIA,10", 3, 65000)
                If Controlla_prelievo(10) = False Then
                    ScriviMessaggio(Messaggio(99), Label1) 'Prelievo fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
                    AttendiTasto()
                    Exit Function
                End If
            End If

            If Inferiore.CD80 = 4 Then
                'Call HiLevSend("w,1,R,0,A,CPCA,5,CPJA,5", "")
                cmdComandoSingolo("w,1,R,0,A,CPCA,5,CPJA,5", 3, 65000)
                If Controlla_prelievo(10) = False Then
                    ScriviMessaggio(Messaggio(98), Label1) 'Prelievo fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
                    AttendiTasto()
                    Exit Function
                End If

                'Call HiLevSend("w,1,R,0,B,CPCA,5,CPJA,5", "")
                cmdComandoSingolo("w,1,R,0,B,CPCA,5,CPJA,5", 3, 65000)
                If Controlla_prelievo(10) = False Then
                    ScriviMessaggio(Messaggio(99), Label1) 'Prelievo fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
                    AttendiTasto()
                    Exit Function
                End If

                'Call HiLevSend("w,1,R,0,C,CPDA,5,CPIA,5", "")
                cmdComandoSingolo("w,1,R,0,C,CPDA,5,CPIA,5", 3, 65000)
                If Controlla_prelievo(10) = False Then
                    ScriviMessaggio(Messaggio(100), Label1) 'Prelievo fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
                    AttendiTasto()
                    Exit Function
                End If

                'Call HiLevSend("w,1,R,0,D,CPDA,5,CPIA,5", "")
                cmdComandoSingolo("w,1,R,0,D,CPDA,5,CPIA,5", 3, 65000)
                If Controlla_prelievo(10) = False Then
                    ScriviMessaggio(Messaggio(101), Label1) 'Prelievo fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
                    AttendiTasto()
                    Exit Function
                End If
            End If

        End If

        ScriviMessaggio(Messaggio(54), Label1) 'Prelievo in corso...
        If Inferiore.categoria = "CM18B" Then
            'Call HiLevSend("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CP**,20", "")
            cmdComandoSingolo("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CP**,20", 3, 65000)
        Else
            'Call HiLevSend("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CPIA,10,CPJA,10", "")
            cmdComandoSingolo("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CPIA,10,CPJA,10", 3, 65000)
        End If

        If Controlla_prelievo(10) = False Then
            ScriviMessaggio(Messaggio(65), Label1) 'Prelievo fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
            AttendiTasto()
            Exit Function
        End If
        ScriviMessaggio(Messaggio(66), Label1) 'Togliere la mazzetta dalla bocchetta di uscita e premere un tasto

        AttendiTasto()
        ScriviMessaggio(Messaggio(54), Label1) 'Prelievo in corso...
        If Inferiore.categoria = "CM18B" Then
            'Call HiLevSend("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CP**,20", "")
            cmdComandoSingolo("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CP**,20", 3, 65000)
        Else
            'Call HiLevSend("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CPIA,10,CPJA,10", "")
            cmdComandoSingolo("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CPIA,10,CPJA,10", 3, 65000)
        End If

        If Controlla_prelievo(0) = False Then
            ScriviMessaggio(Messaggio(65), Label1) 'Prelievo fallito, in caso di jam è possibile operare sulla macchina per risolverlo, premere un tasto per ripetere, Esc per uscire
            AttendiTasto()
            Exit Function
        End If
        Prelievo_30_bn = True
    End Function

    Function Controlla_prelievo(ByVal X As Integer) As Boolean
        'Dim aaa As String = Ricez$
        'Dim prev As Integer = 1
        'Dim act As Integer = 1
        'Dim virgola As Integer = 0
        'Dim stat As String = ""
        'Dim aa As String = ""
        'For a = 1 To Len(aaa)

        '    If Mid$(aaa, a, 1) = "," Then
        '        act = a
        '        virgola = virgola + 1
        '        aa = Mid$(aaa, prev, act - prev)
        '        Select Case virgola
        '            Case 4
        '                stat = Mid$(aaa, prev, act - prev)
        '            Case 5
        '                aa = Mid$(aaa, prev, Len(Ricez$) - (prev - 1))
        '                Exit For
        '        End Select
        '        prev = act + 1
        '    End If
        'Next

        'If X = 10 Then
        '    If Inferiore.categoria = "CM18B" Then
        '        If aa = "CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CP**,20" Then
        '            Controlla_prelievo = True
        '        Else
        '            Controlla_prelievo = False
        '        End If
        '    Else

        '        If stat = "1" Or stat = "101" Or stat = "14" Or stat = "210" Then
        '            Controlla_prelievo = True
        '        Else
        '            Controlla_prelievo = False
        '        End If
        '    End If
        'End If

        If X = 10 Then
            If Inferiore.categoria = "CM18B" Then
                If comandoSingoloRisposta(5) = "10" And
                        comandoSingoloRisposta(7) = "10" And
                        comandoSingoloRisposta(9) = "10" And
                        comandoSingoloRisposta(11) = "10" And
                        comandoSingoloRisposta(13) = "10" And
                        comandoSingoloRisposta(15) = "10" Then
                    Controlla_prelievo = True
                Else
                    Controlla_prelievo = False
                End If
            Else
                If comandoSingoloRisposta(3) = "101" Or
                        comandoSingoloRisposta(3) = "1" Or
                        comandoSingoloRisposta(3) = "14" Or
                        comandoSingoloRisposta(3) = "210" Then
                    Controlla_prelievo = True
                Else
                    Controlla_prelievo = False
                End If
            End If
        End If
        If X = 0 Then

            Controlla_prelievo = True

        End If
    End Function


    Function Svuotamento_CD80() As Boolean

        ScriviMessaggio(Messaggio(102), Label1) 'Aprire la cassaforte, estrarre e svuotare il BOXA, introdurre il barcode del modulo e premere invio.
ins_a:
        Text1.MaxLength = 12
        Text1.Text = ""

        Text1.Visible = True
        Text1.Focus()
        Attendi_Invio()
        If Len(Text1.Text) <> 12 Then
            ScriviMessaggio(Messaggio(106), Label1) 'Barcode non inserito correttamento, introdurlo nuovamente e premere invio
            GoTo ins_a
        End If
        Inferiore.CD80_Matricola_A = Text1.Text

        ScriviMessaggio(Messaggio(103), Label1) 'Inserire il BOXA, estrarre e svuotare il BOXB, introdurre il barcode del modulo e premere invio.
ins_b:
        Text1.Text = ""
        Text1.Visible = True
        Attendi_Invio()
        If Len(Text1.Text) <> 12 Then
            ScriviMessaggio(Messaggio(106), Label1) 'Barcode non inserito correttamento, introdurlo nuovamente e premere invio
            GoTo ins_b
        End If
        Inferiore.CD80_Matricola_B = Text1.Text

        If Inferiore.CD80 = 4 Then
            ScriviMessaggio(Messaggio(104), Label1) 'Inserire il BOXB, estrarre e svuotare il BOXC, introdurre il barcode del modulo e premere invio.
ins_c:
            Text1.Text = ""
            Text1.Visible = True
            Attendi_Invio()
            If Len(Text1.Text) <> 12 Then
                ScriviMessaggio(Messaggio(106), Label1)
                GoTo ins_c
            End If
            Inferiore.CD80_Matricola_C = Text1.Text

            ScriviMessaggio(Messaggio(105), Label1)
ins_d:
            Text1.Text = ""
            Text1.Visible = True
            Attendi_Invio()
            If Len(Text1.Text) <> 12 Then
                ScriviMessaggio(Messaggio(106), Label1)
                GoTo ins_d
            End If
            Inferiore.CD80_Matricola_D = Text1.Text

        End If
        Text1.Visible = False
        ScriviMessaggio(Messaggio(107), Label1) 'Accertarsi che tutti i vassoi siano chiusi, chiudere la cassaforte, attendere il recovery e premere un tasto. 
        AttendiTasto()

        Attendi_recovery()
        Svuotamento_CD80 = True

    End Function


    Function CD80_test() As Boolean
inizio:
        If Inferiore.CD80 > 0 Then
            CD80_test = False
            ScriviMessaggio(Messaggio(31), Label1) 'Aprire la cassaforte ed estrarre il primo vassoio CD80, collegarlo con l'apposita prolunga, verificare che entrambi gli Switch Look siano chiusi e che la maniglia vassoio sia aperta e premere un tasto
            AttendiTasto()

            Traspin()

            'Verifica funzionamento switch bloccaggio cartuccia e switch chiusura vassoio


            'La leva di bloccaggio cartuccia deve essere in posizione di chiusura
            'Switch1:
            '            Call stato_Switch(1)
            '            If CD80_status.Switch_lock_A = True Then 'aperto
            '                ScriviMessaggio(Messaggio(123), Label1) 'Switch lock A atteso chiuso ma rilevato aperto
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo Switch1
            '            End If
            '            If CD80_status.Switch_lock_B = True Then 'aperto
            '                ScriviMessaggio(Messaggio(125), Label1) 'Switch lock B atteso chiuso ma rilevato aperto
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo Switch1
            '            End If





            '            'La leva del vassoio deve essere in posizione di chiusura
            'vassoio:
            '            Scrivi_N("Chiudere la leva del vassoio e premere un tasto")
            '            AttendiTasto()
            '            'Traspsend("E413", 4, 500)
            '            CmdTransparentCommand("E413")
            '            'CD80_status.Vassoio = Traspric.Substring(7, 1)
            '            CD80_status.Vassoio = TransparentAnswer.Substring(7, 1)
            '            If CD80_status.Vassoio = False Then 'aperto
            '                ScriviMessaggio(Messaggio(126), Label1) 'Verifica posizione chiusura vassoio fallita, premere un tasto per ripetere, ESC per uscire
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo vassoio
            '            End If
            'status:
            '            'Traspsend("F4", 1, 500)
            '            'If Traspric <> "05" Then 'Pronto
            '            CmdTransparentCommand("F4")
            '            If TransparentAnswer <> "05" Then
            '                ScriviMessaggio(Messaggio(132) & " " & TransparentAnswer & ". " & Messaggio(133), Label1) ' Attesa risposta 05 ma ricevuta xx . premere un tasto per ripetere, ESC per uscire
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo status
            '            End If

            '            'Aprire la cassaforte ed estrarre il vassoio CD80. Portare le leve in posizione di sblocco cartuccia.
            '            'Inserire il vassoio fino a fondo corsa senza portare la leva del vassoio in posizione di chiusura
            'vassoio2:
            '            Scrivi_N("Portare la leva del vassoio e le leve di bloccaggio delle cartucce in posizione di apertura, e premere un tasto per continuare")
            '            AttendiTasto()
            '            'Traspsend("E413", 4, 500)
            '            'CD80_status.Vassoio = Traspric.Substring(7, 1)
            '            CmdTransparentCommand("E413")
            '            CD80_status.Vassoio = TransparentAnswer.Substring(7, 1)
            '            If CD80_status.Vassoio = True Then 'chiuso
            '                ScriviMessaggio(Messaggio(127), Label1) 'Verifica posizione apertura vassoio fallita, premere un tasto per ripetere, ESC per uscire
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo vassoio2
            '            End If
            'status2:
            '            'Traspsend("F4", 1, 500)
            '            'If Traspric <> "0A" Then 'Vassoio aperto
            '            CmdTransparentCommand("F4")
            '            If TransparentAnswer <> "0A" Then
            '                ScriviMessaggio(Messaggio(134) & " " & TransparentAnswer & Messaggio(133), Label1) ' Attesa risposta 0A ma ricevuta xx . premere un tasto per ripetere, ESC per uscire
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo status2
            '            End If

            '            'Portare la leva del vassoio in posizione di chiusura
            'Switch2:
            '            Scrivi_N("Portare la leva del vassoio in posizione di chiusura e premere un tasto per continuare")
            '            AttendiTasto()
            '            Call stato_Switch(1)
            '            If CD80_status.Switch_lock_A = False Then 'chiuso
            '                ScriviMessaggio(Messaggio(130), Label1) 'Sensore magnetico cassetto A atteso aperto ma rilevato chiuso, premere un tasto per ripetere, ESC per uscire
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo Switch2
            '            End If
            '            If CD80_status.Switch_lock_B = False Then 'chiuso
            '                ScriviMessaggio(Messaggio(131), Label1) 'Sensore magnetico cassetto B atteso aperto ma rilevato chiuso, premere un tasto per ripetere, ESC per uscire
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo Switch2
            '            End If

            'vassoio3:
            '            'Traspsend("E413", 4, 500)
            '            'CD80_status.Vassoio = Traspric.Substring(7, 1)
            '            CmdTransparentCommand("E413")
            '            CD80_status.Vassoio = TransparentAnswer.Substring(7, 1)
            '            If CD80_status.Vassoio = False Then 'aperto
            '                ScriviMessaggio(Messaggio(126), Label1) 'Verifica posizione chiusura vassoio fallita, premere un tasto per ripetere, ESC per uscire
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo vassoio3
            '            End If
            'status3:
            '            'Traspsend("F4", 1, 500)
            '            'If Traspric <> "01" Then 'Idle
            '            CmdTransparentCommand("F4")
            '            If TransparentAnswer <> "01" Then 'Idle
            '                ScriviMessaggio(Messaggio(134) & " " & TransparentAnswer & Messaggio(133), Label1) ' Attesa risposta 0A ma ricevuta xx . premere un tasto per ripetere, ESC per uscire
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo status3
            '            End If
            '            'Estrarre il vassoio CD 80. Portare le leve in posizione di blocco cartuccia.
            '            'Chiudere correttamente il vassoio e la cassaforte.
            '            'Recovery


            '            'Test del sensore magnetico

            'Switch3:
            '            Scrivi_N("Portare le leve di bloccaggio cartuccie e la leva del vassoio in posizione di chiusura, aprire e chiudere la porta per avviare il recovery, e premere un tasto a recovery ultimato")
            '            AttendiTasto()
            '            Call stato_Switch(1)
            '            If CD80_status.Switch_lock_A = False Then 'aperto
            '                ScriviMessaggio(Messaggio(123), Label1) 'Switch lock A atteso chiuso ma rilevato aperto
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo Switch1
            '            End If
            '            If CD80_status.Switch_lock_B = False Then 'aperto
            '                ScriviMessaggio(Messaggio(125), Label1) 'Switch lock B atteso chiuso ma rilevato aperto
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo Switch3
            '            End If

            'Switch4:
            '            Scrivi_N("Portare le leve di bloccaggio cartuccie e la leva del vassoio in posizione di chiusura, aprire e chiudere la porta per avviare il recovery, e premere un tasto a recovery ultimato")
            '            AttendiTasto()
            '            Call stato_Switch(1)
            '            If CD80_status.Switch_lock_A = True Then 'chiuso
            '                ScriviMessaggio(Messaggio(130), Label1) 'Sensore magnetico cassetto A atteso aperto ma rilevato chiuso, premere un tasto per ripetere, ESC per uscire
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo Switch1
            '            End If
            '            If CD80_status.Switch_lock_B = True Then 'chiuso
            '                ScriviMessaggio(Messaggio(131), Label1) 'Sensore magnetico cassetto B atteso aperto ma rilevato chiuso, premere un tasto per ripetere, ESC per uscire
            '                AttendiTasto()
            '                If Tasto = 27 Then Exit Function
            '                GoTo Switch4
            '            End If

















ApertoChiuso:
            ScriviMessaggio(Messaggio(33), Label1) 'Impostare Switch lock A aperto e switch Lock B chiuso con cartucce A e B inserite e premere un tasto
            AttendiTasto()
            Call stato_Switch(1)

            If CD80_status.Switch_lock_A = False Then 'chiuso
                ScriviMessaggio(Messaggio(122), Label1) 'Switch lock A atteso aperto ma rilevato chiuso
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo ApertoChiuso
            End If

            If CD80_status.Switch_lock_B = True Then 'aperto
                ScriviMessaggio(Messaggio(125), Label1) 'Switch lock B atteso chiuso ma rilevato aperto
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo ApertoChiuso
            End If

ChiusoAperto:
            ScriviMessaggio(Messaggio(34), Label1) 'Impostare Switch lock A chiuso e switch Lock B aperto con cartucce A e B inserite e premere un tasto
            AttendiTasto()

            Call stato_Switch(1)
            If CD80_status.Switch_lock_A = True Then 'aperto
                ScriviMessaggio(Messaggio(123), Label1) 'Switch lock A atteso chiuso ma rilevato aperto
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo ChiusoAperto
            End If

            If CD80_status.Switch_lock_B = False Then 'chiuso
                ScriviMessaggio(Messaggio(124), Label1) 'Switch lock B atteso aperto ma rilevato chiuso
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo ChiusoAperto
            End If

            ScriviMessaggio(Messaggio(35), Label1) 'Impostare Switch lock A e switch Lock B aperti con cartucce A e B inserite e premere un tasto
            AttendiTasto()

            Call stato_Switch(1)
            If CD80_status.Switch_lock_A = False Then 'chiuso
                ScriviMessaggio(Messaggio(122), Label1) 'Switch lock A atteso aperto ma rilevato chiuso
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo ChiusoAperto
            End If
            If CD80_status.Switch_lock_B = False Then 'chiuso
                ScriviMessaggio(Messaggio(124), Label1) 'Switch lock B atteso aperto ma rilevato chiuso
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo ChiusoAperto
            End If

ChiusoChiuso:
            ScriviMessaggio(Messaggio(36), Label1) 'Impostare switch lock A e Lock b chiusi, portare la maniglia del vassoio in posizione chiusa(senza inserire il vassoio) e premere un tasto
            AttendiTasto()

            Call Stato_vassoio(1)
            If CD80_status.Vassoio = False Or (CD80_status.Status_A <> "01") Then
                ScriviMessaggio(Messaggio(126), Label1) 'Verifica posizione chiusura vassoio fallita, premere un tasto per ripetere, ESC per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo ChiusoChiuso
            End If


            ScriviMessaggio(Messaggio(37), Label1) 'Portare la maniglia in posizione aperta e premere un tasto
            AttendiTasto()

            Call Stato_vassoio(1)
            If CD80_status.Vassoio = True Or (CD80_status.Status_A <> "0A") Then
                ScriviMessaggio(Messaggio(127), Label1) 'Verifica posizione apertura vassoio fallita, premere un tasto per ripetere, ESC per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo ChiusoChiuso
            End If

manigliaChiusa:
            ScriviMessaggio(Messaggio(38), Label1) 'Portare la maniglia in posizione 'chiusa' e premere un tasto
            AttendiTasto()

            Call Stato_Mag_Sensor(1)
            If CD80_status.Mag_sens_A = False Then 'aperto
                ScriviMessaggio(Messaggio(128), Label1) 'Sensore magnetico cassetto A atteso chiuso ma rilevato aperto, premere un tasto per ripetere, ESC per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo manigliaChiusa
            End If

            If CD80_status.Mag_sens_B = False Then 'aperto
                ScriviMessaggio(Messaggio(129), Label1) 'Sensore magnetico cassetto B atteso chiuso ma rilevato aperto, premere un tasto per ripetere, ESC per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo manigliaChiusa
            End If

matricolaCassettoA:
            ScriviMessaggio(Messaggio(39), Label1) 'Estrarre la cartuccia A, richiudere Switch Lock A, inserire la matricola e premere un invio
            Text1.Text = ""
            Text1.Visible = True
            Text1.Focus()
            Attendi_Invio()
            Inferiore.MatricolaCassA = Text1.Text
            Text1.Visible = False
            Traspout()
cassettoA:
            'HiLevSend("Z,1,A,0," & Inferiore.MatricolaCassA & ",123456")
            'If Ricez.Substring(8) <> "1" And Ricez.Substring(8) <> "101" Then
            cmdComandoSingolo("Z,1,A,0," & Inferiore.MatricolaCassA & ",123456", 4)
            If comandoSingoloRisposta(4) <> "1" And comandoSingoloRisposta(4) <> "101" Then
                'ScriviMessaggio(Ricez.Substring(6) & vbNewLine & Messaggio(121), Label1) 'Fallito inserimento matricola, premere un tasto per ripetere, Esc per uscire
                ScriviMessaggio(comandoSingoloRisposta(4) & vbNewLine & Messaggio(121), Label1) 'Fallito inserimento matricola, premere un tasto per ripetere, Esc per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo matricolaCassettoA
            End If
            Traspin()

MagSersor:
            Call Stato_Mag_Sensor(1)
            If CD80_status.Mag_sens_A = True Then 'chiuso
                ScriviMessaggio(Messaggio(130), Label1) 'Sensore magnetico cassetto A atteso aperto ma rilevato aperto, premere un tasto per ripetere, ESC per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo MagSersor
            End If
            If CD80_status.Mag_sens_B = False Then 'chiuso
                ScriviMessaggio(Messaggio(131), Label1) 'Sensore magnetico cassetto B atteso aperto ma rilevato aperto, premere un tasto per ripetere, ESC per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo MagSersor
            End If

matricolaCassettoB:
            ScriviMessaggio(Messaggio(40), Label1) 'Inserire la cartuccia A, estrarre la cartuccia B, richiudere Switch Lock A e B, inserire la matricola e premere un tasto
            Text1.Text = ""
            Text1.Visible = True
            Text1.Focus()
            Attendi_Invio()
            Inferiore.MatricolaCassB = Text1.Text
            Text1.Visible = False
            Traspout()
cassettoB:
            'HiLevSend("Z,1,B,0," & Inferiore.MatricolaCassB & ",123456")
            'If Ricez.Substring(8) <> "1" And Ricez.Substring(8) <> "101" Then
            cmdComandoSingolo("Z,1,B,0," & Inferiore.MatricolaCassB & ",123456", 4)
            If comandoSingoloRisposta(4) <> "1" And comandoSingoloRisposta(4) <> "101" Then
                ScriviMessaggio(Messaggio(121), Label1) 'Fallito inserimento matricola, premere un tasto per ripetere, Esc per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo matricolaCassettoB
            End If
            Traspin()

magSens1:
            Call Stato_Mag_Sensor(1)
            If CD80_status.Mag_sens_A = False Then 'chiuso
                ScriviMessaggio(Messaggio(128), Label1) 'Sensore magnetico cassetto A atteso chiuso ma rilevato aperto, premere un tasto per ripetere, ESC per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo magSens1
            End If
            If CD80_status.Mag_sens_B = True Then 'aperto
                ScriviMessaggio(Messaggio(129), Label1) 'Sensore magnetico cassetto B atteso chiuso ma rilevato aperto, premere un tasto per ripetere, ESC per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo magSens1
            End If

magSensB:
            ScriviMessaggio(Messaggio(41), Label1) 'Inserire la cartuccia B, richiudere Switch Lock B  e premere un invio
            AttendiTasto()

            Call Stato_Mag_Sensor(1)
            If CD80_status.Mag_sens_A = False Then 'chiuso
                ScriviMessaggio(Messaggio(128), Label1) 'Sensore magnetico cassetto A atteso chiuso ma rilevato aperto, premere un tasto per ripetere, ESC per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo magSensB
            End If
            If CD80_status.Mag_sens_B = False Then 'chiuso
                ScriviMessaggio(Messaggio(131), Label1) 'Sensore magnetico cassetto B atteso aperto ma rilevato aperto, premere un tasto per ripetere, ESC per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo magSensB
            End If

            ScriviMessaggio(Messaggio(42), Label1) 'Test Sorter A

            CmdTransparentCommand("D133")
            CmdTransparentCommand("D40C")

            MySleep(200)

sorter1:
            Call Stato_Sorter_Fork(1)
            If CD80_status.Sorter_fork_A = True Then
                ScriviMessaggio(Messaggio(46), Label1) 'Fallito test Sorter A, premere un tasto per ripetere, Esc per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo sorter1
            End If

            CmdTransparentCommand("D40D")
            MySleep(200)

            Call Stato_Sorter_Fork(1)
            If CD80_status.Sorter_fork_A = False Then
                ScriviMessaggio(Messaggio(46), Label1) 'Fallito test Sorter A, premere un tasto per ripetere, Esc per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo sorter1
            End If

            ScriviMessaggio(Messaggio(43), Label1) 'Test Sorter B
            CmdTransparentCommand("D50C")
            MySleep(200)

sorter2:
            Call Stato_Sorter_Fork(1)
            If CD80_status.Sorter_fork_B = True Then
                ScriviMessaggio(Messaggio(47), Label1) 'Fallito test Sorter B, premere un tasto per ripetere, Esc per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo sorter2
            End If

            CmdTransparentCommand("D50D")
            MySleep(200)

            Call Stato_Sorter_Fork(1)
            If CD80_status.Sorter_fork_B = False Then
                ScriviMessaggio(Messaggio(47), Label1) 'Fallito test Sorter B, premere un tasto per ripetere, Esc per uscire
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo sorter2
            End If

            CD80_test = True
            ScriviMessaggio(Messaggio(114), Label1) 'Spegnere la macchina, sconnettere la prolunga, inserire il vassoio, accendere la macchina, attendere il ciclo di recovery e premere un tasto
            AttendiTasto()

        Else
            CD80_test = True

        End If

    End Function

    Sub stato_Switch(ByVal vassoio As Byte)

        Dim A As String
        Dim B As String
        If vassoio = 1 Then
            A = "4"
            B = "5"
        Else
            A = "6"
            B = "7"
        End If

        'Traspsend("D" & A & "0A", 0, 200)
        'Traspsend("D" & A & "0B", 0, 200)

        'Traspsend("D" & B & "0A", 0, 200)
        'Traspsend("D" & B & "0B", 0, 200)

        'Traspsend("E" & A & "12", 4, 500)
        'CD80_status.Switch_lock_A = Traspric.Substring(7, 1)

        'Traspsend("E" & B & "12", 4, 500)
        'CD80_status.Switch_lock_B = Traspric.Substring(7, 1)

        CmdTransparentCommand("D" & A & "0A")
        CmdTransparentCommand("D" & A & "0B")

        CmdTransparentCommand("D" & B & "0A")
        CmdTransparentCommand("D" & B & "0B")

        CmdTransparentCommand("E" & A & "12")
        CD80_status.Switch_lock_A = TransparentAnswer.Substring(7, 1)

        CmdTransparentCommand("E" & B & "12")
        CD80_status.Switch_lock_B = TransparentAnswer.Substring(7, 1)
    End Sub

    Sub Stato_vassoio(ByVal vassoio As Byte)
        Dim A As String
        Dim B As String
        If vassoio = 1 Then
            A = "4"
            B = "5"
        Else
            A = "6"
            B = "7"
        End If

        'Traspsend("F" & A, 1, 500)
        'CD80_status.Status_A = Traspric

        'Traspsend("F" & B, 1, 500)
        'CD80_status.Status_B = Traspric

        'Traspsend("E" & A & "13", 4, 500)
        'CD80_status.Vassoio = Traspric.Substring(7, 1)

        CmdTransparentCommand("F" & A,)
        CD80_status.Status_A = TransparentAnswer

        CmdTransparentCommand("F" & B,)
        CD80_status.Status_B = TransparentAnswer

        CmdTransparentCommand("E" & A & "13")
        CD80_status.Vassoio = TransparentAnswer.Substring(7, 1)

    End Sub


    Sub Stato_Mag_Sensor(ByVal vassoio As Byte)
        Dim A As String
        Dim B As String
        If vassoio = 1 Then
            A = "4"
            B = "5"
        Else
            A = "6"
            B = "7"
        End If

        'Traspsend("E" & A & "11", 4, 500)
        'CD80_status.Mag_sens_A = Traspric.Substring(7, 1)

        'Traspsend("E" & B & "11", 4, 500)
        'CD80_status.Mag_sens_B = Traspric.Substring(7, 1)

        CmdTransparentCommand("E" & A & "11")
        CD80_status.Mag_sens_A = TransparentAnswer.Substring(7, 1)

        CmdTransparentCommand("E" & B & "11")
        CD80_status.Mag_sens_B = TransparentAnswer.Substring(7, 1)
    End Sub

    Sub Stato_Sorter_Fork(ByVal vassoio As Byte)
        Dim A As String
        Dim B As String
        If vassoio = 1 Then
            A = "4"
            B = "5"
        Else
            A = "6"
            B = "7"
        End If

        'Traspsend("E" & A & "10", 4, 500)
        'CD80_status.Sorter_fork_A = Traspric.Substring(7, 1)

        'Traspsend("E" & B & "10", 4, 500)
        'CD80_status.Sorter_fork_B = Traspric.Substring(7, 1)

        CmdTransparentCommand("E" & A & "10")
        CD80_status.Sorter_fork_A = TransparentAnswer.Substring(7, 1)

        CmdTransparentCommand("E" & B & "10")
        CD80_status.Sorter_fork_B = TransparentAnswer.Substring(7, 1)
    End Sub

    Function Taratura_Foto() As Boolean
inizio:
        ScriviMessaggio(Messaggio(26), Label1) 'Taratura e verifica fotosensori in corso, attendere...
        Dim Griglia As New Grid

        Griglia.Rows = Numero_cassetti_in_test * 2 + 1
        Griglia.Cols = 3
        Griglia.buf(0, 0) = Messaggio(27) 'Fotosensore
        Griglia.buf(1, 0) = Messaggio(28) 'Valore
        Griglia.buf(2, 0) = Messaggio(29) 'Limiti
        Griglia.Colonna_Verde(1)
        Griglia.Colonna_Nera(0)
        Griglia.Colonna_Nera(2)

        Taratura_Foto = True

        Dim kk As Byte

        ReDim Inferiore.Test.cassette_photo(Numero_cassetti_in_test - 1)

        Traspin()

        If Inferiore.CD80 = 2 Then

            'CD80
            'Cassette A
            'Call Traspsend("D408", 0, 500)
            'Call Traspsend("E408", 4, 500)
            CmdTransparentCommand("D408")
            CmdTransparentCommand("E408")
            kk = stringa_invio(TransparentAnswer)
            'kk = stringa_invio(Traspric)
            Inferiore.Test.cassette_photo(0).Limiti.F_In_Box_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_box_min"))
            Inferiore.Test.cassette_photo(0).Limiti.F_In_Box_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_box_max"))
            Inferiore.Test.cassette_photo(0).Foto_in = kk
            Griglia.buf(0, 1) = "Photo IN A"
            Griglia.buf(1, 1) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(0).Limiti.F_In_Box_min Or kk > Inferiore.Test.cassette_photo(0).Limiti.F_In_Box_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 1) = "[" & Inferiore.Test.cassette_photo(0).Limiti.F_In_Box_min & "-" & Inferiore.Test.cassette_photo(0).Limiti.F_In_Box_max & "]"

            'CD80
            'Cassette B
            'Call Traspsend("D508", 0, 500)
            'Call Traspsend("E508", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("D508")
            CmdTransparentCommand("E508")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(1).Limiti.F_In_Box_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_box_min"))
            Inferiore.Test.cassette_photo(1).Limiti.F_In_Box_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_box_max"))
            Inferiore.Test.cassette_photo(1).Foto_in = kk
            Griglia.buf(0, 3) = "Photo IN B"
            Griglia.buf(1, 3) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(1).Limiti.F_In_Box_min Or kk > Inferiore.Test.cassette_photo(1).Limiti.F_In_Box_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 3) = "[" & Inferiore.Test.cassette_photo(1).Limiti.F_In_Box_min & "-" & Inferiore.Test.cassette_photo(1).Limiti.F_In_Box_max & "]"
        Else

            'cassetti normali
            'cassetto A
            'Call Traspsend("D421", 0, 500)
            'Call Traspsend("E421", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("D421")
            CmdTransparentCommand("E421")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(0).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
            Inferiore.Test.cassette_photo(0).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
            Inferiore.Test.cassette_photo(0).Foto_in = kk
            Griglia.buf(0, 1) = "Photo IN A"
            Griglia.buf(1, 1) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(0).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(0).Limiti.F_in_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 1) = "[" & Inferiore.Test.cassette_photo(0).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(0).Limiti.F_in_max & "]"

            'Call Traspsend("D420", 0, 500)
            'Call Traspsend("E420", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("D420")
            CmdTransparentCommand("E420")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(0).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
            Inferiore.Test.cassette_photo(0).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
            Inferiore.Test.cassette_photo(0).Foto_Out = kk
            Griglia.buf(0, 2) = "Photo OUT A"
            Griglia.buf(1, 2) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(0).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(0).Limiti.F_out_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 2) = "[" & Inferiore.Test.cassette_photo(0).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(0).Limiti.F_out_max & "]"

            'cassetti normali
            'cassetto B
            'Call Traspsend("D521", 0, 500)
            'Call Traspsend("E521", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("D521")
            CmdTransparentCommand("E521")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(1).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
            Inferiore.Test.cassette_photo(1).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
            Inferiore.Test.cassette_photo(1).Foto_in = kk
            Griglia.buf(0, 3) = "Photo IN B"
            Griglia.buf(1, 3) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(1).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(1).Limiti.F_in_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 3) = "[" & Inferiore.Test.cassette_photo(1).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(1).Limiti.F_in_max & "]"

            'Call Traspsend("D520", 0, 500)
            'Call Traspsend("E520", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("D520")
            CmdTransparentCommand("E520")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(1).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
            Inferiore.Test.cassette_photo(1).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
            Inferiore.Test.cassette_photo(1).Foto_Out = kk
            Griglia.buf(0, 4) = "Photo OUT B"
            Griglia.buf(1, 4) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(1).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(1).Limiti.F_out_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 4) = "[" & Inferiore.Test.cassette_photo(1).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(1).Limiti.F_out_max & "]"

        End If

        If Inferiore.CD80 = 4 Then

            'CD80
            'Cassette c
            'Call Traspsend("D608", 0, 500)
            'Call Traspsend("E608", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("D608")
            CmdTransparentCommand("E608")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(2).Limiti.F_In_Box_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_box_min"))
            Inferiore.Test.cassette_photo(2).Limiti.F_In_Box_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_box_max"))
            Inferiore.Test.cassette_photo(2).Foto_in = kk
            Griglia.buf(0, 5) = "Photo IN C"
            Griglia.buf(1, 5) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(2).Limiti.F_In_Box_min Or kk > Inferiore.Test.cassette_photo(2).Limiti.F_In_Box_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 5) = "[" & Inferiore.Test.cassette_photo(2).Limiti.F_In_Box_min & "-" & Inferiore.Test.cassette_photo(2).Limiti.F_In_Box_max & "]"

            'CD80
            'Cassette d
            'Call Traspsend("D708", 0, 500)
            'Call Traspsend("E708", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("D708")
            CmdTransparentCommand("E708")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(3).Limiti.F_In_Box_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_box_min"))
            Inferiore.Test.cassette_photo(3).Limiti.F_In_Box_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_box_max"))
            Inferiore.Test.cassette_photo(3).Foto_in = kk
            Griglia.buf(0, 7) = "Photo IN D"
            Griglia.buf(1, 7) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(3).Limiti.F_In_Box_min Or kk > Inferiore.Test.cassette_photo(3).Limiti.F_In_Box_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 6) = "[" & Inferiore.Test.cassette_photo(3).Limiti.F_In_Box_min & "-" & Inferiore.Test.cassette_photo(3).Limiti.F_In_Box_max & "]"
        Else

            'cassetti normali
            'cassetto C
            'Call Traspsend("D621", 0, 500)
            'Call Traspsend("E621", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("D621")
            CmdTransparentCommand("E621")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(2).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
            Inferiore.Test.cassette_photo(2).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
            Inferiore.Test.cassette_photo(2).Foto_in = kk
            Griglia.buf(0, 5) = "Photo IN C"
            Griglia.buf(1, 5) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(2).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(2).Limiti.F_in_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 5) = "[" & Inferiore.Test.cassette_photo(2).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(2).Limiti.F_in_max & "]"

            'Call Traspsend("D620", 0, 500)
            'Call Traspsend("E620", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("D620")
            CmdTransparentCommand("E620")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(2).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
            Inferiore.Test.cassette_photo(2).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
            Inferiore.Test.cassette_photo(2).Foto_Out = kk
            Griglia.buf(0, 6) = "Photo OUT C"
            Griglia.buf(1, 6) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(2).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(2).Limiti.F_out_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 6) = "[" & Inferiore.Test.cassette_photo(2).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(2).Limiti.F_out_max & "]"

            'cassetti normali
            'cassetto D
            'Call Traspsend("D721", 0, 500)
            'Call Traspsend("E721", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("D721")
            CmdTransparentCommand("E721")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(3).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
            Inferiore.Test.cassette_photo(3).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
            Inferiore.Test.cassette_photo(3).Foto_in = kk
            Griglia.buf(0, 7) = "Photo IN D"
            Griglia.buf(1, 7) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(3).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(3).Limiti.F_in_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 7) = "[" & Inferiore.Test.cassette_photo(3).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(3).Limiti.F_in_max & "]"

            'Call Traspsend("D720", 0, 500)
            'Call Traspsend("E720", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("D720")
            CmdTransparentCommand("E720")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(3).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
            Inferiore.Test.cassette_photo(3).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
            Inferiore.Test.cassette_photo(3).Foto_Out = kk
            Griglia.buf(0, 8) = "Photo OUT D"
            Griglia.buf(1, 8) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(3).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(3).Limiti.F_out_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 8) = "[" & Inferiore.Test.cassette_photo(3).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(3).Limiti.F_out_max & "]"

        End If

        'cassetti normali
        'cassetto E
        'Call Traspsend("D821", 0, 500)
        'Call Traspsend("E821", 4, 500)
        'kk = stringa_invio(Traspric)
        CmdTransparentCommand("D821")
        CmdTransparentCommand("E821")
        kk = stringa_invio(TransparentAnswer)
        Inferiore.Test.cassette_photo(4).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
        Inferiore.Test.cassette_photo(4).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
        Inferiore.Test.cassette_photo(4).Foto_in = kk
        Griglia.buf(0, 9) = "Photo IN E"
        Griglia.buf(1, 9) = Val(kk)
        If Val(kk) < Inferiore.Test.cassette_photo(4).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(4).Limiti.F_in_max Then
            Taratura_Foto = False
            Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
        End If
        Griglia.buf(2, 9) = "[" & Inferiore.Test.cassette_photo(4).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(4).Limiti.F_in_max & "]"

        'Call Traspsend("D820", 0, 500)
        'Call Traspsend("E820", 4, 500)
        'kk = stringa_invio(Traspric)
        CmdTransparentCommand("D820")
        CmdTransparentCommand("E820")
        kk = stringa_invio(TransparentAnswer)
        Inferiore.Test.cassette_photo(4).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
        Inferiore.Test.cassette_photo(4).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
        Inferiore.Test.cassette_photo(4).Foto_Out = kk
        Griglia.buf(0, 10) = "Photo OUT E"
        Griglia.buf(1, 10) = Val(kk)
        If Val(kk) < Inferiore.Test.cassette_photo(4).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(4).Limiti.F_out_max Then
            Taratura_Foto = False
            Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
        End If
        Griglia.buf(2, 10) = "[" & Inferiore.Test.cassette_photo(4).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(4).Limiti.F_out_max & "]"

        'cassetti normali
        'cassetto F
        'Call Traspsend("D921", 0, 500)
        'Call Traspsend("E921", 4, 500)
        'kk = stringa_invio(Traspric)
        CmdTransparentCommand("D921")
        CmdTransparentCommand("E921")
        kk = stringa_invio(TransparentAnswer)
        Inferiore.Test.cassette_photo(5).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
        Inferiore.Test.cassette_photo(5).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
        Inferiore.Test.cassette_photo(5).Foto_in = kk
        Griglia.buf(0, 11) = "Photo IN F"
        Griglia.buf(1, 11) = Val(kk)
        If Val(kk) < Inferiore.Test.cassette_photo(5).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(5).Limiti.F_in_max Then
            Taratura_Foto = False
            Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
        End If
        Griglia.buf(2, 11) = "[" & Inferiore.Test.cassette_photo(5).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(5).Limiti.F_in_max & "]"

        'Call Traspsend("D920", 0, 500)
        'Call Traspsend("E920", 4, 500)
        'kk = stringa_invio(Traspric)
        CmdTransparentCommand("D920")
        CmdTransparentCommand("E920")
        kk = stringa_invio(TransparentAnswer)
        Inferiore.Test.cassette_photo(5).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
        Inferiore.Test.cassette_photo(5).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
        Inferiore.Test.cassette_photo(5).Foto_Out = kk
        Griglia.buf(0, 12) = "Photo OUT F"
        Griglia.buf(1, 12) = Val(kk)
        If Val(kk) < Inferiore.Test.cassette_photo(5).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(5).Limiti.F_out_max Then
            Taratura_Foto = False
            Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
        End If
        Griglia.buf(2, 12) = "[" & Inferiore.Test.cassette_photo(5).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(5).Limiti.F_out_max & "]"

        'cassetti normali
        'cassetto E
        'Call Traspsend("D821", 0, 500)
        'Call Traspsend("E821", 4, 500)
        'kk = stringa_invio(Traspric)
        CmdTransparentCommand("D821")
        CmdTransparentCommand("E821")
        kk = stringa_invio(TransparentAnswer)
        Inferiore.Test.cassette_photo(4).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
        Inferiore.Test.cassette_photo(4).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
        Inferiore.Test.cassette_photo(4).Foto_in = kk
        Griglia.buf(0, 9) = "Photo IN E"
        Griglia.buf(1, 9) = Val(kk)
        If Val(kk) < Inferiore.Test.cassette_photo(4).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(4).Limiti.F_in_max Then
            Taratura_Foto = False
            Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
        End If
        Griglia.buf(2, 9) = "[" & Inferiore.Test.cassette_photo(4).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(4).Limiti.F_in_max & "]"

        'Call Traspsend("D820", 0, 500)
        'Call Traspsend("E820", 4, 500)
        'kk = stringa_invio(Traspric)
        CmdTransparentCommand("D820")
        CmdTransparentCommand("E820")
        kk = stringa_invio(TransparentAnswer)
        Inferiore.Test.cassette_photo(4).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
        Inferiore.Test.cassette_photo(4).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
        Inferiore.Test.cassette_photo(4).Foto_Out = kk
        Griglia.buf(0, 10) = "Photo OUT E"
        Griglia.buf(1, 10) = Val(kk)
        If Val(kk) < Inferiore.Test.cassette_photo(4).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(4).Limiti.F_out_max Then
            Taratura_Foto = False
            Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
        End If
        Griglia.buf(2, 10) = "[" & Inferiore.Test.cassette_photo(4).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(4).Limiti.F_out_max & "]"

        'cassetti normali
        'cassetto F
        'Call Traspsend("D921", 0, 500)
        'Call Traspsend("E921", 4, 500)
        'kk = stringa_invio(Traspric)
        CmdTransparentCommand("D921")
        CmdTransparentCommand("E921")
        kk = stringa_invio(TransparentAnswer)
        Inferiore.Test.cassette_photo(5).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
        Inferiore.Test.cassette_photo(5).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
        Inferiore.Test.cassette_photo(5).Foto_in = kk
        Griglia.buf(0, 11) = "Photo IN F"
        Griglia.buf(1, 11) = Val(kk)
        If Val(kk) < Inferiore.Test.cassette_photo(5).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(5).Limiti.F_in_max Then
            Taratura_Foto = False
            Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
        End If
        Griglia.buf(2, 11) = "[" & Inferiore.Test.cassette_photo(5).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(5).Limiti.F_in_max & "]"

        'Call Traspsend("D920", 0, 500)
        'Call Traspsend("E920", 4, 500)
        'kk = stringa_invio(Traspric)
        CmdTransparentCommand("D920")
        CmdTransparentCommand("E920")
        kk = stringa_invio(TransparentAnswer)
        Inferiore.Test.cassette_photo(5).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
        Inferiore.Test.cassette_photo(5).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
        Inferiore.Test.cassette_photo(5).Foto_Out = kk
        Griglia.buf(0, 12) = "Photo OUT F"
        Griglia.buf(1, 12) = Val(kk)
        If Val(kk) < Inferiore.Test.cassette_photo(5).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(5).Limiti.F_out_max Then
            Taratura_Foto = False
            Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
        End If
        Griglia.buf(2, 12) = "[" & Inferiore.Test.cassette_photo(5).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(5).Limiti.F_out_max & "]"

        'cassetti normali
        'cassetto G
        'Call Traspsend("DA21", 0, 500)
        'Call Traspsend("EA21", 4, 500)
        'kk = stringa_invio(Traspric)
        CmdTransparentCommand("DA21")
        CmdTransparentCommand("EA21")
        kk = stringa_invio(TransparentAnswer)
        Inferiore.Test.cassette_photo(6).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
        Inferiore.Test.cassette_photo(6).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
        Inferiore.Test.cassette_photo(6).Foto_in = kk
        Griglia.buf(0, 13) = "Photo IN G"
        Griglia.buf(1, 13) = Val(kk)
        If Val(kk) < Inferiore.Test.cassette_photo(6).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(6).Limiti.F_in_max Then
            Taratura_Foto = False
            Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
        End If
        Griglia.buf(2, 13) = "[" & Inferiore.Test.cassette_photo(6).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(6).Limiti.F_in_max & "]"

        'Call Traspsend("DA20", 0, 500)
        'Call Traspsend("EA20", 4, 500)
        'kk = stringa_invio(Traspric)
        CmdTransparentCommand("DA20")
        CmdTransparentCommand("EA20")
        kk = stringa_invio(TransparentAnswer)
        Inferiore.Test.cassette_photo(6).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
        Inferiore.Test.cassette_photo(6).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
        Inferiore.Test.cassette_photo(6).Foto_Out = kk
        Griglia.buf(0, 14) = "Photo OUT G"
        Griglia.buf(1, 14) = Val(kk)
        If Val(kk) < Inferiore.Test.cassette_photo(6).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(6).Limiti.F_out_max Then
            Taratura_Foto = False
            Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
        End If
        Griglia.buf(2, 14) = "[" & Inferiore.Test.cassette_photo(6).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(6).Limiti.F_out_max & "]"

        'cassetti normali
        'cassetto H
        'Call Traspsend("DB21", 0, 500)
        'Call Traspsend("EB21", 4, 500)
        'kk = stringa_invio(Traspric)
        CmdTransparentCommand("DB21")
        CmdTransparentCommand("EB21")
        kk = stringa_invio(TransparentAnswer)
        Inferiore.Test.cassette_photo(7).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
        Inferiore.Test.cassette_photo(7).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
        Inferiore.Test.cassette_photo(7).Foto_in = kk
        Griglia.buf(0, 15) = "Photo IN H"
        Griglia.buf(1, 15) = Val(kk)
        If Val(kk) < Inferiore.Test.cassette_photo(7).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(7).Limiti.F_in_max Then
            Taratura_Foto = False
            Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
        End If
        Griglia.buf(2, 15) = "[" & Inferiore.Test.cassette_photo(7).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(7).Limiti.F_in_max & "]"

        'Call Traspsend("DB20", 0, 500)
        'Call Traspsend("EB20", 4, 500)
        'kk = stringa_invio(Traspric)
        CmdTransparentCommand("DB20")
        CmdTransparentCommand("EB20")
        kk = stringa_invio(TransparentAnswer)
        Inferiore.Test.cassette_photo(7).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
        Inferiore.Test.cassette_photo(7).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
        Inferiore.Test.cassette_photo(7).Foto_Out = kk
        Griglia.buf(0, 16) = "Photo OUT H"
        Griglia.buf(1, 16) = Val(kk)
        If Val(kk) < Inferiore.Test.cassette_photo(7).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(7).Limiti.F_out_max Then
            Taratura_Foto = False
            Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
        End If
        Griglia.buf(2, 16) = "[" & Inferiore.Test.cassette_photo(7).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(7).Limiti.F_out_max & "]"


        If Numero_cassetti_in_test > 8 Then

            'cassetti normali
            'cassetto I
            'Call Traspsend("DC21", 0, 500)
            'Call Traspsend("EC21", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("DC21")
            CmdTransparentCommand("EC21")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(8).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
            Inferiore.Test.cassette_photo(8).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
            Inferiore.Test.cassette_photo(8).Foto_in = kk
            Griglia.buf(0, 17) = "Photo IN I"
            Griglia.buf(1, 17) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(8).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(8).Limiti.F_in_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 17) = "[" & Inferiore.Test.cassette_photo(8).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(8).Limiti.F_in_max & "]"

            'Call Traspsend("DC20", 0, 500)
            'Call Traspsend("EC20", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("DC20")
            CmdTransparentCommand("EC20")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(8).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
            Inferiore.Test.cassette_photo(8).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
            Inferiore.Test.cassette_photo(8).Foto_Out = kk
            Griglia.buf(0, 18) = "Photo OUT I"
            Griglia.buf(1, 18) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(8).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(8).Limiti.F_out_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 18) = "[" & Inferiore.Test.cassette_photo(8).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(8).Limiti.F_out_max & "]"

            'cassetti normali
            'cassetto J
            'Call Traspsend("DD21", 0, 500)
            'Call Traspsend("ED21", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("DD21")
            CmdTransparentCommand("ED21")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(9).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
            Inferiore.Test.cassette_photo(9).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
            Inferiore.Test.cassette_photo(9).Foto_in = kk
            Griglia.buf(0, 19) = "Photo IN J"
            Griglia.buf(1, 19) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(9).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(9).Limiti.F_in_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 19) = "[" & Inferiore.Test.cassette_photo(9).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(9).Limiti.F_in_max & "]"

            'Call Traspsend("DD20", 0, 500)
            'Call Traspsend("ED20", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("DD20")
            CmdTransparentCommand("ED20")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(9).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
            Inferiore.Test.cassette_photo(9).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
            Inferiore.Test.cassette_photo(9).Foto_Out = kk
            Griglia.buf(0, 20) = "Photo OUT J"
            Griglia.buf(1, 20) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(9).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(9).Limiti.F_out_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 20) = "[" & Inferiore.Test.cassette_photo(9).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(9).Limiti.F_out_max & "]"
        End If

        If Numero_cassetti_in_test > 10 Then

            'cassetti normali
            'cassetto K
            'Call Traspsend("DE21", 0, 500)
            'Call Traspsend("EE21", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("DE21")
            CmdTransparentCommand("EE21")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(10).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
            Inferiore.Test.cassette_photo(10).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
            Inferiore.Test.cassette_photo(10).Foto_in = kk
            Griglia.buf(0, 21) = "Photo IN K"
            Griglia.buf(1, 21) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(10).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(10).Limiti.F_in_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 21) = "[" & Inferiore.Test.cassette_photo(10).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(10).Limiti.F_in_max & "]"

            'Call Traspsend("DE20", 0, 500)
            'Call Traspsend("EE20", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("DE20")
            CmdTransparentCommand("EE20")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(10).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
            Inferiore.Test.cassette_photo(10).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
            Inferiore.Test.cassette_photo(10).Foto_Out = kk
            Griglia.buf(0, 22) = "Photo OUT K"
            Griglia.buf(1, 22) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(10).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(10).Limiti.F_out_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 22) = "[" & Inferiore.Test.cassette_photo(10).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(10).Limiti.F_out_max & "]"

            'cassetti normali
            'cassetto L
            'Call Traspsend("DF21", 0, 500)
            'Call Traspsend("EF21", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("DF21")
            CmdTransparentCommand("EF21")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(11).Limiti.F_in_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_min"))
            Inferiore.Test.cassette_photo(11).Limiti.F_in_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_In_max"))
            Inferiore.Test.cassette_photo(11).Foto_in = kk
            Griglia.buf(0, 23) = "Photo IN L"
            Griglia.buf(1, 23) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(11).Limiti.F_in_min Or kk > Inferiore.Test.cassette_photo(11).Limiti.F_in_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 23) = "[" & Inferiore.Test.cassette_photo(11).Limiti.F_in_min & "-" & Inferiore.Test.cassette_photo(11).Limiti.F_in_max & "]"

            'Call Traspsend("DF20", 0, 500)
            'Call Traspsend("EF20", 4, 500)
            'kk = stringa_invio(Traspric)
            CmdTransparentCommand("DF20")
            CmdTransparentCommand("EF20")
            kk = stringa_invio(TransparentAnswer)
            Inferiore.Test.cassette_photo(11).Limiti.F_out_min = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_min"))
            Inferiore.Test.cassette_photo(11).Limiti.F_out_max = Val(Read_Ini("test_inferiore.ini", "fotosensori", "F_out_max"))
            Inferiore.Test.cassette_photo(11).Foto_Out = kk
            Griglia.buf(0, 24) = "Photo OUT L"
            Griglia.buf(1, 24) = Val(kk)
            If Val(kk) < Inferiore.Test.cassette_photo(11).Limiti.F_out_min Or kk > Inferiore.Test.cassette_photo(11).Limiti.F_out_max Then
                Taratura_Foto = False
                Griglia.buf_color(1, 1) = CMClass.ArcaColor.errore
            End If
            Griglia.buf(2, 24) = "[" & Inferiore.Test.cassette_photo(11).Limiti.F_out_min & "-" & Inferiore.Test.cassette_photo(11).Limiti.F_out_max & "]"
        End If
        Traspout()
        Griglia.font_griglia(Grid1, "arial", 11, 1)
        Griglia.Riempi_Griglia(Grid1)
        Griglia.Ridimensiona_Griglia(Grid1)

        If Taratura_Foto = False Then
            ScriviMessaggio(Messaggio(93), Label1)
            AttendiTasto()
            If Tasto = 27 Then Exit Function
            GoTo inizio
        End If
        Grid1.Visible = False
    End Function

    Function Cassette_Number(Optional ByVal Fissa_4 As Boolean = False) As Boolean
ini:
        ScriviMessaggio(Messaggio(75), Label1) 'Impostazione numero cassetti
        'F,n,11,num_cas

        If Fissa_4 = False Then
            Select Case Inferiore.categoria
                Case "CM18B", "OEM61"
                    Numero_cassetti_in_test = 6
                Case "CM18", "CM20S", "CM18EVO", "CM18SOLO"
                    Numero_cassetti_in_test = 8
                Case "CM18T", "CM18EVOT", "CM20T", "CM18SOLOT"
                    Numero_cassetti_in_test = 12
                Case "CM20"
                    Numero_cassetti_in_test = 10
            End Select
        Else
            Numero_cassetti_in_test = 6
        End If

        'Call HiLevSend("F,1,11," & Trim(Str(Numero_cassetti_in_test)), "")
        cmdComandoSingolo("F,1,11," & Trim(Str(Numero_cassetti_in_test)), 3)
        'If Ricez$.Substring(Ricez$.Length - 2, 2) = ",1" Or Ricez$.Substring(Ricez.Length - 3, 3) = "101" Then
        If comandoSingoloRisposta(3) = ",1" Or comandoSingoloRisposta(3) = "101" Then
            Cassette_Number = True
        Else
            Cassette_Number = False
            ScriviMessaggio(Messaggio(76), Label1)
            AttendiTasto()
            If Tasto = 27 Then Exit Function
            GoTo ini
        End If

        If Inferiore.CD80 > 0 Then
            cmdComandoSingolo("F,1,30," & Trim(Str(Inferiore.CD80)), 3)
            'Call HiLevSend("F,1,30," & Trim(Str(Inferiore.CD80)), "")
        End If

    End Function

    Function Attendi_recovery() As Boolean

        Dim limite As DateTime = DateTime.Now.AddMilliseconds(50000)

        MySleep(5000)
        Do While Not Open_R(False) = True
            Application.DoEvents()
            If DateTime.Now > limite Then
                Attendi_recovery = False
                Exit Function
            End If
            MySleep(500)
        Loop

        Attendi_recovery = True

    End Function
    Function Open_R(control As Boolean)
        Ricez = ""
        Open_R = False
        'Call HiLevSend("O,1,R,123456", "")
        cmdComandoSingolo("O,1,R,123456", 3)
        If control = False Then
            'If  Microsoft.VisualBasic.Right(Ricez, 2) <> "102" Then
            If comandoSingoloRisposta(3) <> "102" And comandoSingoloRisposta(3) <> "4" Then
                Open_R = True
            End If
        Else
            'If Microsoft.VisualBasic.Right(Ricez, 2) = ",1" Or Microsoft.VisualBasic.Right(Ricez, 3) = "101" Then
            If comandoSingoloRisposta(3) = "1" Or comandoSingoloRisposta(3) = "101" Then
                Open_R = True
            End If
        End If
    End Function

    Function Open_L(control As Boolean)
        Ricez = ""
        Open_L = False
        'Call HiLevSend("O,1,R,123456", "")
        cmdComandoSingolo("O,1,L,123456", 3)
        If control = False Then
            'If Microsoft.VisualBasic.Right(Ricez, 2) <> "102" Then
            If comandoSingoloRisposta(3) <> "102" Then
                Open_L = True
            End If
        Else
            'If Microsoft.VisualBasic.Right(Ricez, 2) = ",1" Or Microsoft.VisualBasic.Right(Ricez, 3) = "101" Then
            If comandoSingoloRisposta(3) = "1" Or comandoSingoloRisposta(3) = "101" Then
                Open_L = True
            End If
        End If
    End Function

    Function Close_R() As Boolean
        'Call HiLevSend("C,1,R", "")
        cmdComandoSingolo("C,1,R", 2)
        Return True
    End Function

    Function Close_L() As Boolean
        'Call HiLevSend("C,1,L", "")
        cmdComandoSingolo("C,1,L", 2)
        Return True
    End Function

    Function Sfoglia_200bn(ByVal In_cassaforte As Boolean) As Boolean
ini:

        ScriviMessaggio(Messaggio(86), Label1)
        AttendiTasto()
        Deposito_ALL(Not In_cassaforte)
        If Deposito.RC <> 1 And Deposito.RC <> 101 Then
            ScriviMessaggio(Messaggio(87), Label1)
            AttendiTasto()
            If Tasto = 27 Then Exit Function
            GoTo ini
        End If

        Inferiore.Test.Bn_riconosciute = Deposito.Banconote_tot
        Inferiore.Test.Bn_rifiutate = Deposito.Banconote_UNFit

        If Deposito.Banconote_UNFit > 15 And Deposito.Banconote_tot > 100 Then
            Sfoglia_200bn = True
        Else
            Sfoglia_200bn = False
            ScriviMessaggio(Messaggio(87), Label1)
            AttendiTasto()
            If Tasto = 27 Then Exit Function
            GoTo ini
        End If

    End Function

    Function Assigne_Cassette() As Boolean
rip:
        ScriviMessaggio(Messaggio(97), Label1) 'Assign cassettes in corso, attendere la fine del del recovery, il programma ripartirà da solo      
        'Ricez = ""
        'Call HiLevSend("A,1", "")
        Assigne_Cassette = False


        'Dim aa As String = Ricez

        'Dim aaa As String = Ricez$ & ","
        'Dim prev As Integer = 1
        'Dim act As Integer = 1
        'Dim virgola As Integer = 0
        'Dim rc As String
        'For a = 1 To Len(aaa)
        '    If Mid$(aaa, a, 1) = "," Then
        '        act = a
        '        virgola = virgola + 1
        '        aa = Mid$(aaa, prev, act - prev)
        '        Select Case virgola
        '            Case 4
        '                rc = aa
        '        End Select
        '        prev = act + 1
        '    End If
        'Next
        cmdComandoSingolo("A,1", 2, 60000)

        If comandoSingoloRisposta.Length > 1 Then
            If comandoSingoloRisposta(2) = "101" Or comandoSingoloRisposta(2) = "1" Then
                Assigne_Cassette = True
            Else
                Assigne_Cassette = False
                ScriviMessaggio(Messaggio(96), Label1)
                AttendiTasto()
                If Tasto = 27 Then Exit Function
                GoTo rip
            End If

        Else
            Assigne_Cassette = False
            ScriviMessaggio(Messaggio(96), Label1)
            AttendiTasto()
            If Tasto = 27 Then Exit Function
            GoTo rip
        End If
        'If rc = "0" Then
        '    Assigne_Cassette = True
        'Else
        '    Assigne_Cassette = False
        '    ScriviMessaggio(Messaggio(96), Label1)
        '    AttendiTasto()
        '    If Tasto = 27 Then Exit Function
        '    GoTo rip
        'End If

    End Function

    Function Balance_set() As Boolean

        Dim Conn_String As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source =" & Database_path & Database_name

        Dim cn As New OleDbConnection(Conn_String)
        cn.Open()

        Dim cmd As New OleDb.OleDbCommand("SELECT id_nome_unit_config FROM tblman WHERE id_prodotto = '" & Inferiore.Codice_Macchina & "' and id_cliente = " & Inferiore.Codice_cliente & " and id_modulo = 'FW_PRODOTTO' and tipo_fw = 4", cn)

        Dim dr As OleDbDataReader = cmd.ExecuteReader
        dr.Read()
        Dim Id_Unit As Integer = dr(0)

        Dim sql As String
        sql = "SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " & Trim(Str(Id_Unit))

        Dim cmd1 As New OleDbCommand(sql, cn)
        Dim conta = 0
        dr = cmd1.ExecuteReader
        dr.Read()

        '' creo il primo...F,n,7,nnnn

        '2'0x0001 = view date & time on display
        '3'0x0002 = reserved
        '4'0x0004 = reserved
        '5'0x0008 = reserved
        '6'0x0010 = reserved
        '7'0x0020 = reserved
        '8'0x0040 = balanced cassette handling
        '9'0x0080 = allarm1 handling
        ''0x0100 = use delay class dispensing
        ''0x0200 = reserved
        ''0x0400 = data & time on display in format AM and PM
        ''0x0800 = use UNFIT in SAFE
        ''0x1000 = reserved
        ''0x2000 = reserved
        ''0x4000 = reserved
        ''0x8000 = enable JOURNAL LOG
        Dim nnnn As UShort
        nnnn = 0

        If dr(2) = True Then nnnn = nnnn + &H1
        nnnn = nnnn + &H40
        If dr(4) = True Then nnnn = nnnn + &H80
        If dr(5) = True Then nnnn = nnnn + &H100
        If dr(6) = True Then nnnn = nnnn + &H400
        If dr(7) = True Then nnnn = nnnn + &H800
        If dr(8) = True Then nnnn = nnnn + &H4000 + &H4000

        Call HiLevSend("F,1,7," & dec_hex(nnnn), "")

        If Ricez$.Substring(Ricez$.Length - 2, 2) = ",1" Or Ricez$.Substring(Ricez.Length - 3, 3) = "101" Then
            Balance_set = True
        Else
            Balance_set = False
            Exit Function
        End If

    End Function

    'Private Sub ListBox1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ListBox1.KeyUp
    '    Tasto = e.KeyCode
    'End Sub



    Private Sub WithdrawalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WithdrawalToolStripMenuItem.Click
        Mini_prelievo()
    End Sub

    Private Sub Text1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Text1.KeyPress

    End Sub

    Private Sub Text1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Text1.KeyUp, ListBox1.KeyUp, Me.KeyUp
        Tasto = e.KeyCode
        tastoPremuto = Tasto
    End Sub

    Private Sub Text1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text1.TextChanged

    End Sub

    Private Sub Principale_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        End
    End Sub

    Private Sub MenuStrip1_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles MenuStrip1.ItemClicked

    End Sub
End Class
